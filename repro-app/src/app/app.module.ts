import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';


import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersService } from './users/users.service';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UpdateFormComponent } from './users/update-form/update-form.component';
import { UsersFirebaseComponent } from './users/users-firebase/users-firebase.component';
import { UsersNavigationComponent } from './users/users-navigation/users-navigation.component';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  { path: ''                    , component: LoginComponent              },
  { path: 'users'               , component: UsersComponent              },
  { path: 'login'               , component: LoginComponent              },
  { path: 'users-firebase'      , component: UsersFirebaseComponent      },
  { path: 'products'            , component: ProductsComponent           },
  { path: 'update-form/:id'     , component: UpdateFormComponent         },
  { path: '**'                  , component: NotFoundComponent           }  
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    UsersComponent,
    ProductsComponent,
    NavigationComponent,
    UsersFormComponent,
    UpdateFormComponent,
    UsersFirebaseComponent,
    UsersNavigationComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(      
      appRoutes,
      { enableTracing: false }
    )
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }