import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../../environments/environment';

@Injectable()
export class UsersService {
  // Variables
  http:Http;

  getUsers(){  
	return this.http.get(environment.url + '/users');
  }

  postUser(data){
    let params = new HttpParams().append('name',data.name).append('phone',data.phone);      
    let options =  {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'    
      })
    }      
    return this.http.post(environment.url + '/users', params.toString(), options);
  }

  deleteMessage(key){      
    return this.http.delete(environment.url + '/users/delete/' + key);
  }

  getUser(key){  
     return this.http.get(environment.url + '/users/' + key);
  }

  updateUser(data){       
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })   
    };

    let params = new HttpParams().append('name',data.name).append('phone',data.phone);  

    return this.http.put(environment.url + '/users/' + data.id,params.toString(), options);      
  }

  getUsersFireBase(){
    return this.db.list('/Users').valueChanges();
  }

  login(credentials){
     let options = {
        headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
        })
     }
    let  params = new HttpParams().append('name', credentials.user).append('password',credentials.password);
    return this.http.post('http://localhost/angular/exampleTest2/slim/login', params.toString(),options).map(response=>{ 
      let success = response.json().success;
      if (success == true){
        localStorage.setItem('auth','true');
      }else{
        localStorage.setItem('auth','false');        
      }
   });
  }


  constructor(http:Http, private db:AngularFireDatabase) { 
 	this.http = http; 
  }

}
