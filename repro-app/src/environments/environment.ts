// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

//הגדרה מקומית localhost
export const environment = {
  production: true,
  url: 'http://localhost/angular/exampleTest2/slim',
  firebase:{
    apiKey: "AIzaSyBKjbw_t9RDnWU3YQRpLxKHQ0HpijEP1Kc",
    authDomain: "messages-f1eee.firebaseapp.com",
    databaseURL: "https://messages-f1eee.firebaseio.com",
    projectId: "messages-f1eee",
    storageBucket: "messages-f1eee.appspot.com",
    messagingSenderId: "383964082538"
  }
};