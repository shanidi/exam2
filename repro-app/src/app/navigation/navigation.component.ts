import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private router:Router) {}

  logout(){ 
      localStorage.removeItem('auth');      
      this.router.navigate(['/login']);
  }

  ngOnInit() {
  	var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      this.router.navigate(['/users']);
    }else{
      this.router.navigate(['/login']);
    }
  }

}
